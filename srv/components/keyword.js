'use strict';

/**
 * Keywords are universal to emotions and conversations, etc
 * This model allows to link a keyword to either an emotion or a conversation.
 * For now, I don't see a reason why a keyword cannot be shared between an emotion and a conversation.
 */
class Keyword {

    constructor(db, keyword) {
        this.db = db;
        this.id = undefined;
        this.keyword = keyword;
        this.conversation_id = undefined;
        this.emotion_id = undefined;
    }

    /**
     * Call this to persist to database;
     */
    persist() {
        this.db.persist_keyword(this.keyword, this.conversation_id, this.emotion_id);
    }

    /**
     * Associate an emotion with this keyword.
     * @param {int} id Emotion ID
     */
    set_emotion_id(id) {
        this.emotion_id = id;
    }

    /**
     * Associate a conversation with this keyword.
     * @param {int} id Conversation ID
     */
    set_conversation_id(id) {
        this.conversation_id = id;
    }


}

module.exports = Keyword;