'use strict';

let natural = require('natural');
let tokenizer = new natural.WordTokenizer();

/**
 * Wrapper for NLP NodeNatural/Natural
 * @see https://github.com/NaturalNode/natural
 */
class Natural {

    constructor(logger) {
        this.logger = logger;
        this.logger.info('Construction of Natural occurred.');
    }

}

module.exports = Natural;