'use strict';

let Natural = require('srv/components/nlp/natural');
let Keyword = require('srv/components/keyword');
let Conversation = require('srv/components/conversation');


/**
 * Primary Message processing.
 * This is the first line of action post-Router
 */
class Processor {

    constructor(logger, chat_logger, database) {
        this.logger = logger;
        this.chat_logger = chat_logger;
        this.nlp = new Natural(logger);
        this.database = database;

        this.logger.info('Construction of Processor occurred.');
    }

    query(message) {
        this.chat_logger.info(message);

        console.log('-========== processor query message =======-');
        console.log(message);
        console.log('-========== processor query message =======-');

        // 1. parse message into the needful format
        let timestamp = message.timestamp;
        let speaker = message.speaker;
        let input = message.message;

        console.log('----A');
        let convo = new Conversation(this.database);

        console.log('----B');



        //  === process main (log everything that makes sense) ===
        // - NLP time... break it, parse it.
        // - determine context (topic[noun phrase], speaker? etc)
        // - determine the emotion, sentiment, mood, etc of the input (greeting?) classify/classifier
        // - push input to brain/
        // - persist input data where/as useful
        // - persist brain response where/as useful
        // - build useful human response... send back to speaker.

        return "DEV RESPONSE FROM PROCESSOR: " + message;
    }

}

module.exports = Processor;