'use strict';

class Conversation {

    constructor(db) {
        this.db = db;

        this.id = undefined;
        this.keyword_ids = [];
        this.emotion_id = undefined;
        this.created = undefined;

        this.create();
    }

    /**
     * Create a conversation, awaits the promise fulfiment, the updates the model.
     */
    create() {
        this.db.create_conversation()
            .then(id => {
                this.id = id;
                this.update_model();
            }).catch(err => {
                throw err;
            });
    }

    get_conversation_id() {
        return this.id;
    }

    add_context_id(keyword_id) {
        this.db.conversation_add_keyword(this.id, keyword_id);
        this.update_model();
    }

    set_emotion_id(emotion_id) {
        this.db.conversation_set_emotion(this.id, emotion_id);
        this.update_model();
    }

    /**
     * Updates this object with the database row associated to this instance id.
     */
    update_model() {
        this.db.retrieve_conversation(this.id, (err, row) => {
            if (err) {
                // @todo
            }

            this.emotion_id = row.emotion_id;
            this.keyword_ids = row.keyword_ids;
            this.created = row.created;
        });
    }

}

module.exports = Conversation;