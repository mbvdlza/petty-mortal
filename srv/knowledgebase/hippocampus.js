'use strict';

let sqlite3 = require('sqlite3');

const TABLE_KEYWORD = "keyword";
const TABLE_CONVERSATION = "conversation";
const TABLE_EMOTION = "emotion";

/**
 * @TODO - no time. BUT: Async/Promise this stuff......... !!
 */

class Hippocampus {

    constructor(db_file, logger) {
        this.db = undefined;
        this.db_file = db_file;
        this.logger = logger;
        this.logger.info('Construction of Hippocampus occurred.');
        this.initialise();
    }

    initialise() {
        this.logger.info('Initialising Hippocampus');
        this.db = new sqlite3.Database(this.db_file, 'OPEN_READWRITE | OPEN_CREATE');
        // Cannot get the callback of sqlite3 constructor to fire properly... need to look at this.

        // clean up any tables that require startup hygiene
        this.cleanup();
    }

    /**
     * Any code pertaining to 'on startup' table cleanups.
     */
    cleanup() {
        this.logger.info('Hippocampus cleanup operations');
    }

    /**
     * ========================= KEYWORD ============================
     */

    /**
     * Persists data to Keyword table
     * @param {String} keyword Keyword to insert
     * @param {Int} conversation_id Conversation ID to associate with
     * @param {Int} emotion_id Emotion ID to associate with
     */
    persist_keyword(keyword, conversation_id, emotion_id) {
        this.logger.info('Hippocampus: Persisting Keyword.');
        let stmt = this.db.prepare("INSERT INTO " + TABLE_KEYWORD + " VALUES (NULL,?,?,?)");
        stmt.run(keyword, conversation_id, emotion_id);
    }

    /**
     * ========================= CONVERSATION ============================
     */

    /**
     * Creates a conversation record, with a created date, and no other useful values.
     * Conversations are alive, and dynamically updated as they progress.
     * @returns {Promise}
     */
    create_conversation() {
        this.logger.info('Hippocampus: Creating Conversation.');
        return new Promise((resolve, reject) => {
            this.db.run("INSERT INTO " + TABLE_CONVERSATION + " VALUES (NULL,$default_keywords,NULL,$created)", {
                $default_keywords: JSON.stringify([]),
                $created: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
            }, function(err, data) {
                if (err) {
                    // @todo
                    reject(new Error('Insert for creating a new conversation did not finish successfully.'));

                }
                resolve(this.lastID);
            });
        });
    }

    /**
     * Updates the Conversation table with an additional keyword
     * @param {Int} id Conversation ID to update
     * @param {Int} keyword Keyword ID to add to this conversation
     */
    conversation_add_keyword(id, keyword_id) {
        this.logger.info('Hippocampus: Updating Conversation %d - adding keyword id %d', id, keyword_id);
        this.conversation_get_keywords(id, (keywords) => {
            keywords.push(keyword_id);

            // filter duplicates. Nicer ways of doing this exists... like not inserting dupes in the first place
            keywords = keywords.filter(function(elem, pos) {
                return keywords.indexOf(elem) == pos;
            });

            let stmt = this.db.prepare("UPDATE " + TABLE_CONVERSATION + " SET keyword_ids = ? WHERE id = ?");
            stmt.run(JSON.stringify(keywords), id);
            stmt.finalize();
        });
    }

    /**
     * Get the keywords associated with conversation ID.
     * @param {Int} id The conversation ID to retrieve keywords for.
     * @returns {number[]}
     */
    conversation_get_keywords(id, next) {
        this.logger.info('Hippocampus: Getting Keywords for Conversation %d', id);
        // get the json of keyword id's.
        this.db.get("SELECT * FROM " + TABLE_CONVERSATION + " WHERE id = " + id, function(err, row) {
            if (err) {
                throw new Error('Vague error, cant find conversation id.... fix me');
            }

            next(JSON.parse(row.keyword_ids));
        });
    }

    /**
     * Updates/Sets the emotion ID for a conversation.
     * @param {Int} id The conversation ID to update or set.
     * @param {Int} emotion_id The emotion ID to set this to.
     */
    conversation_set_emotion(id, emotion_id) {
        this.logger.info('Hippocampus: Setting Emotion for Conversation %d to emotion_id %d', id, emotion_id);
        let stmt = this.db.prepare("UPDATE " + TABLE_CONVERSATION + " SET emotion_id = ? WHERE id = ?");
        stmt.run(emotion_id, id);
        stmt.finalize();
    }

    retrieve_conversation(id, next) {
        this.logger.info('Hippocampus: Retrieving conversation id %d', id);
        this.db.get("SELECT * FROM " + TABLE_CONVERSATION + " WHERE id = " + id, function(err, row) {
            if (err) {
                throw new Error('Retrieve error, cant find conversation id....');
            }

            next(null, row);
        });
    }

    /**
     * ========================= EMOTION ============================
     */

    emotion_set_name(id, emotion) {
        this.logger.info('Hippocampus: Setting Name for Emotion %d to %s', id, emotion);
        let stmt = this.db.prepare("UPDATE " + TABLE_EMOTION + " SET name = ? WHERE id = ?");
        stmt.run(emotion, id);
        stmt.finalize();
    }

    emotion_get_keywords(id, next) {
        this.logger.info('Hippocampus: Getting Keywords for Emotion id %d', id);

        // get the json of keyword id's.
        this.db.get("SELECT * FROM " + TABLE_EMOTION + " WHERE id = " + id, function(err, row) {
            if (err) {
                throw new Error('Vague error, cant find emotion id.... fix me');
            }

            next(JSON.parse(row.keyword_ids));
        });
    }

    /**
     * Add a keyword id to this associated emotion.
     * @param {Int} id The emotion id associated with the keyword
     * @param {Int} keyword_id The keyword id to add to this emotion
     */
    emotion_add_keyword(id, keyword_id) {
        this.logger.info('Hippocampus: Updating Emotion %d - adding keyword id %d', id, keyword_id);
        this.emotion_get_keywords(id, (keywords) => {
            keywords.push(keyword_id);

            // filter duplicates. Nicer ways of doing this exists... like not inserting dupes in the first place
            keywords = keywords.filter(function(elem, pos) {
                return keywords.indexOf(elem) == pos;
            });

            let stmt = this.db.prepare("UPDATE " + TABLE_EMOTION + " SET keyword_ids = ? WHERE id = ?");
            stmt.run(JSON.stringify(keywords), id);
            stmt.finalize();
        });
    }

}

module.exports = Hippocampus;
