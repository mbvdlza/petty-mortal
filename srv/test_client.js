'use strict';

var net = require('net'),
    JsonSocket = require('json-socket');

var port = 8099;
var host = '127.0.0.1';
var socket = new JsonSocket(new net.Socket());

try {
    socket.connect(port, host);


    socket.on('connect', function () { //Don't send until we're connected
        socket.sendMessage(
            {
                message: 'Run to the Jungle!',
                speaker: 'Bob'
            });

        socket.on('message', function (message) {
            console.log('The result is: ' + message.result);
        });
    });
} catch (err) {
    console.log('Problems!!!');
    console.log(err);
}
