'use strict';

var joi = require('joi');

module.exports = {
    speaker: joi.string().required().max(64),
    message: joi.string().required()
};
