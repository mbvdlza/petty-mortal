'use strict';

let net = require('net');
let JsonSocket = require('json-socket');

class MortalServer {

    /**
     * MortalServer Constructor.
     * @param {Integer} port Port Number to listen on.
     */
    constructor(port, router, logger) {
        this.logger = logger;

        this.logger.info('MortalServer init on port %d', port);
        this.router = router;
        this.port = port;
        this.server = net.createServer();
        this.server.listen(this.port);

        this.server.on('connection', (netSocket) => {
            this.onConnection(netSocket);
        });
    }


    /**
     * Handling of the 'on message' event.
     * @param {json} message Incoming message in Json format.
     */
    onMessage(message) {
        this.logger.debug('Server message handler gets a message', message);
        this.router.accept(message)
            .then((response) => {
                this.socket.sendEndMessage({ result: response });
            }).catch((error) => {
                this.logger.error('Server message handler caught an error', error);
                this.socket.sendEndMessage({ result: 'yikes we need catch handlers' });    // @todo - wrap this into a generic error json
        });
    }


    /**
     * OnConnection handler.
     * Wraps plain net sockets into JsonSockets + other connection handling.
     * @param {net.Socket} netSocket Instance of a plain old 'net.Socket' socket.
     */
    onConnection(netSocket) {
        this.logger.debug('onConnection...');

        this.socket = new JsonSocket(netSocket);
        this.socket.on('message', (message) => {
            this.onMessage(message);
        });
    }

}

module.exports = MortalServer;
