'use strict';

const joi = require('joi');
const querySchema = require('srv/validation/query_schema');

/**
 * Server routing.
 */
class Router {

    /**
     * Create an instance of Router, and takes all usable classes as injection.
     * @todo: at some point, use the rest parameter to build up list of components?
     */
    constructor(logger, processor) {
        this.logger = logger;
        this.processor = processor;
        this.logger.info('Construction of Router occurred.');
    }

    /**
     * Decompiles query into the callable names, invokes and returns the result.
     * @param {JSON} query Should contain two keys, worker and callee, optionally 'param'
     * @returns {Promise}
     */
    accept(query) {
        this.logger.debug('QUERY reception: ', query);
        return new Promise((resolve, reject) => {
            this.validate(query)
                .then((result) => {
                    this.logger.debug('Router: Query validated OK');

                    // Extract query parts and preprocess them if needed
                    const message = this.preprocess_message(result);

                    // Push the message to the processor. Next time we hear from you, everything will be done.
                    const response = this.processor.query(message);

                    return resolve(response);

                }).catch((error) => {
                    this.logger.error('Router:accept() caught an error. Rejecting query', query);
                    reject(error);
            });

        });
    }

    /**
     * Preprocessor: Message
     * @param {String } message Incoming message to preprocess.
     * @returns {String}
     */
    preprocess_message(message) {
        message.timestamp = new Date()
            .toISOString()
            .slice(0, 19)
            .replace('T', ' ');

        return message;
    }

    /**
     * Validate the query against the schema. This should move to it's own class soon.
     * @param {JSON} validatee The json query to validate.
     * @returns {Promise}
     */
    validate(validatee) {
        return new Promise((resolve, reject) => {
            joi.validate(validatee, querySchema, function (err, value) {
                if (!err) {
                    resolve(value);
                } else {
                    reject(err);
                }
            });
        });
    }

}

module.exports = Router;
