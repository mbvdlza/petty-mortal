'use strict';

var winston = require('winston');

class MortalLog {
    
    constructor(log_file, level) {
        this.winston = new (winston.Logger)({
            transports: [
              new (winston.transports.Console)()
            ]
        });

        if (log_file) {
        	this.winston.add(winston.transports.File,
        		{ filename: log_file });        
        }

        if (level) {
            this.winston.level = level;
        }

        return this.winston;
    }

}

module.exports = MortalLog;
