'use strict';

let Server = require('srv/server');
let Router = require('srv/router');
let MortalLog = require('srv/logger/mortal_log');
let processor = require('srv/components/processor');
let Hippocampus = require('srv/knowledgebase/hippocampus');

// this must go config... env.
const SERVER_PORT = 8099;

// LOGGING CONFIG (must also go to env soon!)
const LOG_DIR = '/tmp/';
const LOG_FILE = LOG_DIR + 'petty_mortal_main.log';
const LOG_CHAT_FILE = LOG_DIR + 'petty_mortal_chat.log';
const LOG_LEVEL = 'debug';

// DB config
const DB_FILE = './hippocampus.db';

/**
 * Components to inject into the router
 * Need a pattern here, desperate for a pattern.
 */
let logger = new MortalLog(LOG_FILE, LOG_LEVEL);
let chat_logger = new MortalLog(LOG_CHAT_FILE, 'info');

/**
 * Fire up database/hippocampus.
 */
let database = new Hippocampus(DB_FILE, logger);

/**
 * Instantiate the router, and inject the components.
 * @type {Router}
 */
let router = new Router(logger, new processor(logger, chat_logger, database));

new Server(SERVER_PORT, router, logger);
